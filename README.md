[![pipeline status](https://gitlab.com/ansible-roles9/users/badges/development/pipeline.svg)](https://gitlab.com/ansible-roles9/users/-/commits/development) ![Project version](https://img.shields.io/gitlab/v/tag/ansible-roles9/users)![Project license](https://img.shields.io/gitlab/license/ansible-roles9/users)

# Role USERS
El objetivo del rol es crear y realizar la configuración inicial de los usuarios necesarios.

## Dependencias y particularidades
Este rol no tiene dependencias

Para el caso de la funcionalidad **home**, es importante declarar el valor de manera absoluta y no relativa, es decir, la ruta al directorio deberá de empezar y terminar con **/**.

Para el caso de la funcionalidad **umask**, el rol ajustará el parámetro ***umask*** en el fichero **.bash_profile** localizado en el directorio **home** del usuario, el mismo debe de estar compuesto por 4 dígitos numéricos comprendidos del 0 al 7; siendo el primero el ***sticky-bit***, por lo general deberá de ser 0 si es que no aplica.

Para la funcionalidad **secret**, el rol asignará una contraseña al usuario en cuestión cifrando esta con el algoritmo SHA512, el mismo compatible con sistemas ***Linux***.

## Variables
Debajo una pequeña tabla que podría definir brevemente las características de cada variable sobre ***user_group***:

|  | Mandatoria | Opcional | Criterio | Tipo |
|--|--|--|--|--|
| name | X | - | - | Cadena [KS] |
| gid | - | X | Si no se indica, se usará el primero disponible | Numérico |

Debajo una pequeña tabla que podría definir brevemente las características de cada variable sobre ***user_config***:

|  | Mandatoria | Opcional | Criterio | Tipo |
|--|--|--|--|--|
| username | X | - | - | Cadena [KS] |
| uid | - | X | Si no se indica, se usará el primero disponible | Numérico |
| home | - | X | Si no se indica, se usará el formato /home/[username]/ | Cadena [KS] |
| group | - | X | Si no se indica, se usará el grupo por defecto segun la distribución, **wheel** para basados en RHEL y **sudo** para basados en Debian | Cadena [KS] |
| groups | - | X | El usuario será configurado para solo responder a los grupos declarados | Cadena separada por comas [KS] |
| umask | - | X | Debe de estar compuesto por 4 dígitos [0-7] (0022) | Numérico [KS] |
| secret | - | X | Si no se indica, se prescindirá su uso | Cadena [KS] |
| shell | - | X | Si no se indica, se usará /bin/bash | Cadena [KS] |
| password_policy | - | X | Si no se indica, se aplicará el criterio 1-99999 | Boleano |
| comments | X | - | - | Cadena [KS] |

Debajo una pequeña tabla que podría definir brevemente las características de cada variable sobre ***user_cronenv***:

|  | Mandatoria | Opcional | Criterio | Tipo |
|--|--|--|--|--|
| username | X | - | - | Cadena [KS] |
| key | X | - | - | Cadena [KS] |
| value | X | - | - | Cadena [KS] |

Debajo una pequeña tabla que podría definir brevemente las características de cada variable sobre ***user_cronconfig***:

|  | Mandatoria | Opcional | Criterio | Tipo |
|--|--|--|--|--|
| username | X | - | - | Cadena [KS] |
| name | X | - | - | Cadena [KS] |
| weekday | - | X | Si no se indica, se usará (*) | Numérico  |
| minute | X | - | Si no se indica, se usará (*) | Numérico |
| hour | X | - | Si no se indica, se usará (*) | Numérico |
| day | X | - | Si no se indica, se usará (*) | Numérico |
| job | X | - | - | Cadena [KS] |

Existe una variable llamada **force**, la función de la misma por defecto es **true**, el objetivo de esta variable es la de controlar el error humano al momento de realizar cambios en usuarios, el comportamiento según el valor es el siguiente:

|  | Criterio | Tipo |
|--|--|--|
| true | Intentará realizar los cambios necesarios para abastecer la solicitud | Boleano |
| false | Si cualquiera de los usuarios existe, provocará un fallo | Boleano  |

## Funcionamiento
La sintaxis de los parámetros que recibe debería de ser:

- **user_group**: Recibe el listado en formato array de los grupos que hay que crear. Se muestra un ejemplo a continuación:
```
{name: 'grupo1', gid: '123456'}
```

- **user_config**: Recibe el listado en formato array de los usuarios que hay que crear. Se muestra un ejemplo a continuación:
```
{username: 'Agar1', uid: '123456', group: 'ungrupo', groups: 'grupo1, grupo2', home: '/opt/Agar1/,' umask: '0022', shell: '/bin/bash', password_policy: true, comments: 'Usuario Aplicación GAR'}
```

- **user_cronenv**: Recibe los parámetros de entorno para la creación del fichero cron por defecto de los usuarios deseados. A continuación se muestra un ejemplo de la estructura que debe seguir:
```
{username: 'Pgar1', key: 'MAIL', value: 'rot'}
```

- **user_cronconfig**: Recibe los parámetros para la creación del fichero cron por defecto de los usuarios deseados. A continuación se muestra un ejemplo de la estructura que debe seguir:
```
{ username: 'Pgar1', name: 'Tarea para sincronizar', weekday: '2', minute: '45', hour: '10',  day: '3-5', job: 'echo hola' }
```

## Leyenda
* NKS => No key sensitive
* KS => Key sensitive
