# [1.3.0](/Ansible_Roles/users/compare/1.3.0...main)
* CICD migraton from Jenkins to GitlabCI
* Update exceptions for *handlers* in order to work with *Gitlab*

# [1.2.0](/Ansible_Roles/users/compare/1.2.0...main)
* Se configuran las versiones del `CHANGELOG.md` para que sean revisables por la herramienta de comparación de **Gitea**
* Se actualiza el *Jenkinsfile* para con las variables que son necesarias por la versión actual del **Shared libraries**
* Se actualiza *meta* indicando la versión de `ansible` máxima soportada.

# [1.1.0](/Ansible_Roles/users/compare/1.1.0...main)
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados
* Se reemplaza la nomenclatura en el compilador virtual para la arquitectura `linux/arm64/v8`, ahora se denomina `linux/arm64` por **buildx**, reemplazamos valor en variable `platform` del *Jenkinsfile*

# [1.0.0](/Ansible_Roles/users/compare/1.0.0...main)
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `ansiblePipeline`
* Se ajusta el `README` para que muestre información relevante
* Se ajusta el `.editorconfig`, `.gitignore` con las nuevas reglas bajo el estándar de plantilla

# [0.1.1](/Ansible_Roles/users/compare/0.1.1...main)
* Actualizamos `Jenkinsfile` para adaptarnos a la nueva versión del vars `ansibleActions`[jenkins-shared-libraries]

# [0.1.0](/Ansible_Roles/users/compare/0.1.0...main)
* Se inicializa el *CHANGELOG* para un mejor seguimiento de los cambios
* Se activa el **update_cache** para cada vez que se haga la instalación de requerimientos
* Se actualiza el *Jenkinsfile* con la configuración adaptada para las nuevas librerías
